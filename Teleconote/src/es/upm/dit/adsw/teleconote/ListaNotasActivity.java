package es.upm.dit.adsw.teleconote;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

/**
 * @author -- Pablo Sánchez Belinchón --
 * @version 20130501
 */
public class ListaNotasActivity extends ListActivity {
	public static final String REQUEST_CODE = "requestCode";
	public static final int CREAR_NOTA = 0;
	public static final int BORRAR_NOTA = 1;
	public static final int MODIFICAR_NOTA = 2;

	private final String TAG = ListaNotasActivity.class.getSimpleName();

	private NotaDbAdaptador notaDbAdaptador;
	private Cursor notasCursor;
	private SimpleCursorAdapter cursorAdapter;

	private int indiceTitulo;
	private int indiceContenido;
	private int indiceCategoria;
	private int indiceCifrado;
	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");
		setContentView(R.layout.lista_notas);
		notaDbAdaptador = new NotaDbAdaptador(this);
		// Rellenamos la lista
		notasCursor = notaDbAdaptador.recuperaTodasLasNotas();
		startManagingCursor(notasCursor); // le dice a la actividad que gestione
		// el cursor
		indiceTitulo = notasCursor
				.getColumnIndexOrThrow(NotaDbAdaptador.COL_TITULO);
		indiceCategoria = notasCursor
				.getColumnIndexOrThrow(NotaDbAdaptador.COL_CATEGORIA);
		indiceContenido = notasCursor
				.getColumnIndexOrThrow(NotaDbAdaptador.COL_CONTENIDO);
		indiceCifrado = notasCursor
				.getColumnIndexOrThrow(NotaDbAdaptador.COL_CIFRADO);
		// Crea un array para indicar los campos que queremos mostrar en la
		// lista
		String[] from = new String[] { NotaDbAdaptador.COL_TITULO,
				NotaDbAdaptador.COL_CATEGORIA };
		// y un array con los campos de la plantilla que queremos asignarles
		int[] to = new int[] { android.R.id.text1, android.R.id.text2 };

		// Creamos un SimpleCursorAdapter y escogemos una plantilla de android
		// para mostrar 2 campos
		cursorAdapter = new SimpleCursorAdapter(this,
				android.R.layout.simple_list_item_2, notasCursor, from, to);
		setListAdapter(cursorAdapter); // MIRAR JAVADOC DEL SIMPLECURSORADAPTER
	}

	private void actualizaLista() {
		notasCursor.requery();
		cursorAdapter.notifyDataSetChanged();
	}

	@Override
	public void onStart() {
		super.onStart();
		Log.d(TAG, "onStart");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "onResume");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "onStop");
	}

	// Cuando pulsamos a un elemento de la lista
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Log.i(TAG, "onListItemClick");
		Cursor cursor = notasCursor;
		cursor.moveToPosition(position);
		boolean cifrado = int2boolean(cursor.getInt(indiceCifrado));
		Nota nota = new Nota(cursor.getString(indiceTitulo),
				cursor.getString(indiceContenido),
				cursor.getString(indiceCategoria), cifrado);
		String categorias[] = notaDbAdaptador.recuperaCategorias();
		Log.i(TAG,"cat.length="+categorias.length);
		Intent intento = new Intent(this, DetalleNotaActivity.class);
		Bundle extras = new Bundle();
		extras.putSerializable(Nota.NOTA, nota);
		extras.putStringArray("LISTA_CATEGORIAS",
				categorias);
		Log.i(TAG,"EXTRAS cat.length="+extras.getStringArray("LISTA_CATEGORIAS").length);
		extras.putLong(NotaDbAdaptador.COL_ID, id);
		extras.putInt(REQUEST_CODE, MODIFICAR_NOTA);
		intento.putExtras(extras);
		Log.i(TAG,"Listanotas nota= "+nota);
		startActivityForResult(intento, MODIFICAR_NOTA);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_lista, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.info: {
			Log.i(TAG, "MENU: info seleccionado");
			Toast.makeText(this, es.upm.dit.adsw.teleconote.R.string.info,
					Toast.LENGTH_SHORT).show();
			return true;
		}
		case R.id.add: {
			String categorias[] = notaDbAdaptador.recuperaCategorias();
			Log.i(TAG, "cat.length=" + categorias.length);
			Intent intento = new Intent(this, DetalleNotaActivity.class);
			Bundle extras = new Bundle();
			extras.putStringArray("LISTA_CATEGORIAS", categorias);
			extras.putInt(REQUEST_CODE, CREAR_NOTA);
			intento.putExtras(extras);
			startActivityForResult(intento, CREAR_NOTA);
			return true;
		}

		case R.id.delete: {
			Log.i(TAG, "MENU: delete seleccionado");
			borrarLista();
			return true;
		}
		default: {
			Log.w(TAG, "Opción desconocida " + item.getItemId());
			return false;
		}
		}
	}

	private void borrarLista() {
		AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
		dialogo.setMessage(R.string.alerta_borrar_lista);
		dialogo.setPositiveButton(R.string.si,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						notaDbAdaptador.borraTodasLasNotas();
						actualizaLista();
					}
				});
		dialogo.setNegativeButton(R.string.no,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.w(TAG, "Cancelado borrar lista");

					}
				});
		AlertDialog confirma = dialogo.create();
		confirma.show();
	}

	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (resultCode == RESULT_CANCELED) {
			Log.i(TAG, "Cancelado");
			return;
		}
		Bundle extras = intent.getExtras();
		if (extras == null) {
			Toast.makeText(this, getString(R.string.error_detalle),
					Toast.LENGTH_SHORT).show();
			return;
		}
		
		Nota nota = (Nota) extras.getSerializable(Nota.NOTA);

		switch (requestCode) {
		case CREAR_NOTA: {
			Log.i(TAG, "Crear nota " + nota);
			notaDbAdaptador.creaNota(nota);
			cursorAdapter.notifyDataSetChanged();
			actualizaLista();
			break;
		}
		case MODIFICAR_NOTA: {
			Long id = extras.getLong(NotaDbAdaptador.COL_ID);
			Log.i(TAG, "Modifica nota " + id);
			if (id == null) {
				Log.e(TAG, getString(R.string.error_detalle));
				finish();
			}

			notaDbAdaptador.actualizaNota(id, nota);
			actualizaLista();
			break;
		}
		default: {
			Log.e(TAG, "Opción no conocida " + requestCode);
		}
		}

	}

	private boolean int2boolean(int i) {
		return (i == 0);
	}

}
