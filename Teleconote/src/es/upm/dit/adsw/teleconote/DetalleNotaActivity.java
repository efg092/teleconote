package es.upm.dit.adsw.teleconote;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * @author -- Pablo Sánchez Belinchón --
 * @version 20130501
 */

public class DetalleNotaActivity extends Activity {
	private static final String TAG = DetalleNotaActivity.class.getSimpleName();

	private ImageView imagenCifrado;
	private EditText editTitulo;
	private EditText editContenido;
	private AutoCompleteTextView autoEditCategoria;
	private Button buttonGuardar;
	private Button buttonCancelar;
	private Button buttonCifrar;
	private long id;
	private Nota nota;
	private String clave = null;
	private final String CLAVEOK = "clave";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");
		setTheme(android.R.style.Theme); // bug in dropdown
		setContentView(R.layout.detalle_nota);

		editTitulo = (EditText) findViewById(R.id.editText_titulo);
		editContenido = (EditText) findViewById(R.id.editText_contenido);
		buttonGuardar = (Button) findViewById(R.id.guardar);
		buttonGuardar.setOnClickListener(new MiButtonGuardarOnClickListener());
		buttonCancelar = (Button) findViewById(R.id.cancelar);
		buttonCancelar.setOnClickListener(new MiButtonCancelOnClickListener());
		buttonCifrar = (Button) findViewById(R.id.cifrar);
		imagenCifrado = (ImageView) findViewById(R.id.imageCifrado);
		buttonCifrar.setOnClickListener(new MiButtonCifrarOnClickListener());
		autoEditCategoria = (AutoCompleteTextView) findViewById(R.id.editTextAuto_cat);

		// Recogemos la info que nos pasa ListaNotasActivity
		Bundle extras = getIntent().getExtras();
		if (extras == null) {
			Toast.makeText(this, (R.string.error_detalle), Toast.LENGTH_SHORT)
					.show();
			Log.e(TAG, "Error.- No hay extras");
			finish();
		}
		Log.d(TAG, "extras recuperados");

		this.nota = (Nota) extras.getSerializable(Nota.NOTA);
		Log.i(TAG, "detalle, nota = " + this.nota);

		// Funcionalidad del autocompleteTextView -> SALEN REPETIDAS...
		String categorias[] = extras.getStringArray("LISTA_CATEGORIAS");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, categorias);
		autoEditCategoria.setAdapter(adapter);

		int requestCode = extras.getInt(ListaNotasActivity.REQUEST_CODE);
		if (requestCode == ListaNotasActivity.MODIFICAR_NOTA) {
			refrescarVista();
			id = extras.getLong(NotaDbAdaptador.COL_ID);

			Log.d(TAG, "Editando " + this.nota);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		Log.d(TAG, "onStart");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "onResume");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d(TAG, "onStop");
	}

	private class MiButtonGuardarOnClickListener implements
			View.OnClickListener {
		@Override
		public void onClick(View v) {

			Log.d(TAG, "onClick Boton Guardar");
			Intent intento = new Intent();
			Bundle extras = new Bundle();
			extras.putLong(NotaDbAdaptador.COL_ID, id);
			refrescarNota();

			Log.i(TAG, "nota = " + nota);
			extras.putSerializable(Nota.NOTA, DetalleNotaActivity.this.nota);
			intento.putExtras(extras);
			setResult(RESULT_OK, intento);
			// Antes de salir, borramos la clave para que nos la pida
			clave = null;
			finish();
		}
	}

	private class MiButtonCancelOnClickListener implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			Log.d(TAG, "onClick Boton cancelar");
			setResult(RESULT_CANCELED);
			finish();
		}
	}

	private class MiButtonCifrarOnClickListener implements View.OnClickListener {
		@Override
		public void onClick(final View v) {
			Log.d(TAG, "onClick Boton cifrar");
			Log.d(TAG,"clave : "+clave);
			if (clave == null||!clave.contentEquals(CLAVEOK)) {
				// <Dialogo para pedir la clave + AsyncTask>
				AlertDialog.Builder dialogoCifrado = new AlertDialog.Builder(
						DetalleNotaActivity.this);
				final EditText entrada = new EditText(DetalleNotaActivity.this);
				entrada.setInputType(InputType.TYPE_CLASS_TEXT
						| InputType.TYPE_TEXT_VARIATION_PASSWORD);
				entrada.setSelection(entrada.getText().length());
				dialogoCifrado.setView(entrada);
				dialogoCifrado.setMessage("Introduce la clave de cifrado");
				// Boton de aceptar
				dialogoCifrado.setPositiveButton("Aceptar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								clave = entrada.getText().toString();
								if(clave.contentEquals(CLAVEOK))
								new cifrarTask().execute();
								else{
									Toast.makeText(
											DetalleNotaActivity.this,
											"Clave incorrecta",
											Toast.LENGTH_SHORT).show();
								}
							}
						});

				// Boton de cancelar
				dialogoCifrado.setNegativeButton("Cancelar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								return;
							}
						});
				dialogoCifrado.show();
				// </Dialogo para pedir la clave + AsyncTask>

			} else {
				new cifrarTask().execute();
			}
			Log.d(TAG, "Clave = " + clave);
		}
	}

	/**
	 * AsyncTask<Params, Progress, Result> – Params: tipo de parámetros enviados
	 * a la tarea para su ejecución – Progress – tipo de las unidades de
	 * progreso publicadas durante su ejecución – Result – resultado de la
	 * ejecución de la tarea
	 */
	private class cifrarTask extends AsyncTask<Void, Integer, Boolean> {
		private final ProgressDialog barra = new ProgressDialog(
				DetalleNotaActivity.this);

		protected void onPreExecute() {
			Log.d(TAG, "onPreExecute");
			barra.setTitle("Realizando labores de cifrado");
			barra.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			barra.show();
			refrescarNota();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			Log.d(TAG, "doInBackground");
			String contenido = nota.getContenido();
			String resultado;
			Cifrador cifrador = new Cifrador(clave);
			if (nota.isCifrado()) {
				Log.i(TAG, "Descifrando" + contenido);
				StringBuilder buffer = new StringBuilder(contenido.length());
				try {
					for (int i = 0; i < contenido.length(); i++) {
						char c2;
						c2 = cifrador.descifraCaracter(contenido.charAt(i));
						buffer.append(c2);
						try {
							Log.d(TAG, "Veces dormido:" + (i + 1));
							Thread.sleep(100 * i);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						publishProgress(100 * i / contenido.length());
					}
					resultado = buffer.toString();
					Log.d(TAG, "Resultado = " + resultado);
					nota.setContenido(resultado);
					nota.setCifrado(false);

				} catch (IllegalArgumentException e) {
					Log.e(TAG, "ERROR:Caracter no descifrable");
					resultado = contenido;
					return true;
				}
				resultado = buffer.toString();
				nota.setContenido(resultado);
				nota.setCifrado(false);
			} else if (!nota.isCifrado()) {
				Log.i(TAG, "Cifrando" + contenido);
				StringBuilder buffer = new StringBuilder(contenido.length());

				try {
					for (int i = 0; i < contenido.length(); i++) {
						char c2;
						c2 = cifrador.cifraCaracter(contenido.charAt(i));
						buffer.append(c2);
						try {
							Log.d(TAG, "Veces dormido:" + (i + 1));
							Thread.sleep(100 * i);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						publishProgress(100 * i / contenido.length());
					}
					resultado = buffer.toString();
					Log.d(TAG, "Resultado = " + resultado);
					nota.setContenido(resultado);
					nota.setCifrado(true);

				} catch (IllegalArgumentException e) {
					Log.e(TAG, "ERROR:Caracter no cifrable");
					resultado = contenido;
					return true;
				}

			} else {
				throw new IllegalArgumentException();
			}
			return false;
		}

		protected void onProgressUpdate(Integer... value) {
			super.onProgressUpdate(value);
			barra.show();
			barra.setProgress(value[0]);
		}

		protected void onPostExecute(final Boolean error) {
			if (barra.isShowing()) {
				barra.dismiss();
			}
			refrescarVista();
			if (error) {
				Toast.makeText(
						DetalleNotaActivity.this,
						"ERROR: El mensaje contiene caracteres no cifrables/no descifrables",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	/**
	 * Refresca la vista (imagen, texto del boton, contenido y
	 * categoría) con los contenidos this.nota
	 */
	private void refrescarVista() {
		Log.d(TAG, "refrescarVita");
		editTitulo.setText(this.nota.getTitulo());
		editContenido.setText(this.nota.getContenido());
		autoEditCategoria.setText(this.nota.getCategoria());
		if (this.nota.isCifrado()) {
			buttonCifrar.setText(R.string.descifrar);
			imagenCifrado.setImageResource(R.drawable.ic_menu_cifrar);
		}
		if (!this.nota.isCifrado()) {
			buttonCifrar.setText(R.string.cifrar);
			imagenCifrado.setImageResource(R.drawable.ic_menu_descifrar);
		}
	}

	/**
	 * Introduce en this.nota los valores en pantalla
	 */
	private void refrescarNota() {
		Log.d(TAG, "refrescarNota");
		String titulo = editTitulo.getText().toString();
		String contenido = editContenido.getText().toString();
		String categoria = autoEditCategoria.getText().toString();
		boolean estaCifrado = isCifrada();
		this.nota = new Nota(titulo, contenido, categoria, estaCifrado);
	}

	/**
	 * @return true si en el textview aparece la nota como cifrada
			   false si en el textview aparece la nota como no cifrada
	 */
	private boolean isCifrada() {
		Log.d(TAG, "isCifrada");
		if (buttonCifrar.getText() == getString(R.string.descifrar))
			return true;
		else
			return false;
	}
}
